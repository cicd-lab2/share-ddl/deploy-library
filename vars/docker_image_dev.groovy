def call(Map config =[:]){
    def COMMIT_HASH = sh(script:'git rev-parse --short=8 HEAD ', returnStdout: true).trim()
    def TIMESTAMP = sh(script: "echo `date '+%Y-%m-%d'`", returnStdout: true).trim()
    def NAME_BRANCH = sh(script:' git rev-parse --abbrev-ref HEAD', returnStdout: true).trim()
    def image_name_format_dev =  "'${repo_url_dev}'/'${image_name}':${COMMIT_HASH}-${TIMESTAMP}".replaceAll(" ", "-")
    def newest_image_dev = "'${image_name_format_dev}'"
    sh (""" 
        docker build -t '${image_name}':latest . 
        docker tag '${image_name}':latest  $newest_image_dev
        docker tag '${image_name}':latest '${repo_url_dev}'/'${image_name}':latest
        """)
    withCredentials([usernamePassword(credentialsId: "${env.credentialsId_nexus}",  passwordVariable: "${env.password_nexus}", usernameVariable: "${env.user_nexus}" )]) {
        sh ("""
        echo 'login nexus'
        docker login -u '${env.user_nexus}' -p '${env.password_nexus}'  '${repo_url_dev}'
        docker push $newest_image_dev
        docker push '${repo_url_dev}'/'${image_name}':latest
        """)
    }
    
    // return COMMIT_HASH
    // docker push '${config.repo}'/petclinic-dl:125f5ffe'
}   
