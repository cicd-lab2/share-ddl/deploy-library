def call (){
    sh " scp dev-compose.yml  ${Remote_User}@${Remote_Host}:/app_dev/  "
    sshagent(["${Agent}"]) {
        sh """
            ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                mkdir /app_dev
                docker login -u "${env.user_nexus}" -p "${env.password_nexus}"  ${Repo_url_dev}
                docker pull ${image_dev}
                mkdir -p dev_logs && sudo chown nobody:nobody dev_logs 
                image_dev=${image_dev} Port_dev=${Port_dev}  Container_dev=${Container_dev} docker-compose -f /app_dev/dev-compose.yml up -d  
            '
        """
        def deployStatusDev = sh(script: "ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} docker inspect --format '{{.State.Status}}' '${Container_dev}' ",returnStdout: true).trim()      
        if (deployStatusDev != 'exited') {
            // sshagent(["${Agent}"]) {
            sh """
                ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                echo 'add tag stable for image in local'
                docker tag '${Image_dev}' '${Repo_url_dev}'/petclinic-dl-dev:stable
                '
            """
            // }
        } else {
                // sshagent(["${Agent}"]) {
                sh """
                    ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                    image_dev=${Image_dev} Port_dev=${Port_dev}  Container_dev=${Container_dev} docker-compose  -f /app_dev/dev-compose.yml down 
                    image_dev='${Repo_url_dev}'/petclinic-dl-dev:stable  Port_dev=${Port_dev}  Container_dev=${Container_dev} docker-compose  -f /app_dev/dev-compose.yml up -d 
                    '               
                """
                // }
        }  
    }
}

