def call(String BRANCH_NAME) {
        
    withCredentials([gitUsernamePassword(credentialsId: 'root-gitlab')]) {
        def TIMESTAMP = sh(script: "echo `date '+%Y-%m-%d'`", returnStdout: true).trim()
        def shortCommitHash = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
        def tagName
        if (env.BRANCH_NAME == 'uat') {
            tagName = "uat-${shortCommitHash}-${TIMESTAMP}"
        }      
        else if(env.BRANCH_NAME == 'main') {
            tagName = "main-${shortCommitHash}-${TIMESTAMP}"
        }
        sh "git tag ${tagName} || true"
        sh "git push origin ${tagName}"
    }
}
