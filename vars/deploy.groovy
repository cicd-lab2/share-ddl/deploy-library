def call(Map config= [:]){
    def command = "ansible-playbook -i ${config.inventoryPath} ${config.playbookPath} \
    --extra-vars \"service=${config.serviceNames} port=${config.servicePorts}\""
    sh(script: command, returnStatus: true)
}