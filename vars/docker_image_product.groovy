def call(Map config =[:]){
    def COMMIT_HASH = sh(script:'git rev-parse --short=8 HEAD ', returnStdout: true).trim()
    // def BRANCH_NAME = sh(script:' git rev-parse --abbrev-ref HEAD', returnStdout: true).trim()
    def TIMESTAMP = sh(script: "echo `date '+%Y-%m-%d'`", returnStdout: true).trim()
    def NAME_BRANCH = sh(script:' git rev-parse --abbrev-ref HEAD', returnStdout: true).trim()
    def image_name_format_product =  "'${repo_url_dev}'/'${image_name}':${COMMIT_HASH}-${TIMESTAMP}".replaceAll(" ", "-")
    def newest_image_product = "'${image_name_format_product}'"
    sh (""" 
        docker tag '${image_name}':latest  $newest_image_product
        docker tag '${image_name}':latest '${repo_url_product}'/'${image_name}':latest
        """)
    withCredentials([usernamePassword(credentialsId: "${env.credentialsId_nexus}",  passwordVariable: "${env.password_nexus}", usernameVariable: "${env.user_nexus}" )]) {
        sh ("""
        echo 'login nexus'
        docker login -u '${env.user_nexus}' -p '${env.password_nexus}'  '${repo_url_product}'
        docker push  $newest_image_product
        docker push '${repo_url_product}'/'${image_name}':latest
        """)
    }
}   
