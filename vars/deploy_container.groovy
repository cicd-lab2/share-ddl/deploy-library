def call (Map config=[:]){ 
    // def log_directory = '/app'
    def danglingImages = sh(script: 'docker images -f dangling=true -q',returnStdout: true).trim()
    // def deployStatusProduct = sh(script: "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker inspect --format '{{.State.Status}}' ${config.container_name_product}", returnStdout: true).trim()
    // def body = load "resources/value.groovy"
    // def agent = body.agent
    // def image_name = body.image_name
    // def port = body.port
    // def container_name = body.container_name
    
    sshagent(["${config.agent}"]) {
        // writeFile file: '.env', text: "image_name=${config.image_name}\nport=${config.port}\ncontainer_name=${config.container_name}"
        // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} echo  .env"
        // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} 'docker login -u admin -p 123  ${config.repo_url}' "
        // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker pull ${config.image_name}"
        sh "scp ./dev-compose.yml  ${config.remoteUser}@${config.remoteHost}:/dev_project/dev-compose.yml "
        // // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker run -d --name ${config.container_name} -p '${config.port}':8080   '${config.image_name}'"
        // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker-compose --env-file /app/.env -f /app/petclinic-compose.yml up -d "
        // // sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost}  'docker logs ${config.container_name} >> $log_directory/${config.container_name}.log ' "       
        sh """
            ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} ' 
                docker login -u admin -p 123  ${config.repo_url_dev}
                docker pull ${config.image_dev}
                image_dev=${config.image_dev} dev_port=${config.dev_port} docker-compose  -f /dev_project/dev-compose.yml up -d  
            '
        """
        def deployStatusDev = sh(script: "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker inspect --format '{{.State.Status}}' dev_project_petclinic-dl-product_1",returnStdout: true).trim()
        if (deployStatusDev != 'exited') {
            sh("""
                echo 'add tag stable for image in local'
                
                docker tag '${config.image_dev}' '${config.repo_url_dev}'/petclinic-dl-dev:stable
                docker rmi  $danglingImages
            """)
        } else {
            sh ("""
                image_dev=${config.image_dev} docker-compose  -f /dev_project/dev-compose.yml down 
                
                
                image_dev='${config.repo_url_dev}'/petclinic-dl-dev:stable dev_port=${config.dev_port} docker-compose  -f /dev_project/dev-compose.yml up -d 
                docker rmi  $danglingImages 
                """)
                //  sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker-compose --env-file /app/.env -f /app/petclinic-compose.yml up -d "
        }
    }
}
// docker tag '${config.image_product}' '${config.repo_url_product}'/petclinic-dl-product:stable
// image_product=${config.image_product} docker-compose  -f /product/product-compose.yml down  
// image_product='${config.repo_url_product}'/petclinic-dl-product:stable docker-compose  -f /product/product-compose.yml up -d
//  image_product=${config.image_product} docker-compose  -f /product/product-compose.yml up -d  
            //   docker pull ${config.image_product}