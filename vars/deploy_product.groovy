def call (){
    sh "scp ./product-compose.yml  ${Remote_User}@${Remote_Host}:/app_product/"
    sshagent(["${Agent}"]) {
        sh """
            ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                mkdir /app_product
                docker login -u "${env.user_nexus}" -p "${env.password_nexus}"  ${Repo_url_product}
                docker pull ${Image_product}
                mkdir -p product_logs && sudo chown nobody:nobody product_logs 
                Image_product=${Image_product}  Port_product=${Port_product}  Container_product=${Container_product} docker-compose -f /app_product/product-compose.yml up -d  
            '
        """
        def deployStatusProduct = sh(script: "ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} docker inspect --format '{{.State.Status}}' '${Container_product}' ",returnStdout: true).trim()      
        if (deployStatusProduct != 'exited') {
            // sshagent(["${Agent}"]) {
            sh """
                ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                echo 'add tag stable for image in local'
                docker tag '${Image_product}' '${Repo_url_product}'/petclinic-dl-product:stable
                '
            """
            // }
        } else {
                // sshagent(["${Agent}"]) {
                sh """
                    ssh -o StrictHostKeyChecking=no ${Remote_User}@${Remote_Host} ' 
                    Image_product=${Image_product} Port_product=${Port_product}  Container_product=${Container_product} docker-compose  -f /app_product/product-compose.yml down 
                    Image_product='${Repo_url_product}'/petclinic-dl-product:stable Port_product=${Port_product}  Container_product=${Container_product} docker-compose  -f /app_product/product-compose.yml up -d 
                    '               
                """
                // }
        }  
    }
}

