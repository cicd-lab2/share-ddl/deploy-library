def call (Map config=[:]){
    def danglingImages = sh(script: 'docker images -f dangling=true -q',returnStdout: true).trim()
    def deployStatus = sh(script: "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker inspect --format '{{.State.Status}}' ${config.container_name}",returnStdout: true).trim()
    if (deployStatus != 'exited') {
        sh("""
            echo 'add tag stable for image in local'
            docker tag '${config.image_name}' '${config.repo_url}'/petclinic-dl:stable
            docker rmi  $danglingImages
        """)
    } else {
        sh ("""
            echo 'Container ${config.image_name} has exited so push tag to nexus when the newest had failed'
            docker tag '${config.repo_url}'/petclinic-dl:stable '${config.image_name}' 
            docker push '${config.image_name}'
        """)
        sshagent(["${config.agent}"]) {
            sh """
            ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} '
            docker-compose -f /app/dev-compose.yml down 
            docker-compose  -f /app/dev-compose.yml up -d
            docker rmi  $danglingImages '
            """
            //  sh "ssh -o StrictHostKeyChecking=no ${config.remoteUser}@${config.remoteHost} docker-compose --env-file /app/.env -f /app/petclinic-compose.yml up -d "
        }
    }
}